import React, { useEffect, useState } from 'react';
import {Army, Battle} from "./functions/army";
import {Battles} from "./components/battles";
import {Footer} from "./components/footer";
import {RenderArmies} from "./components/armies";
import {ArmySelected} from "./components/armySelected";
import {ArmyForTraining} from "./components/armyForTraining";
import {BattleHistory} from "./components/battleHistory";

const quantityOfTeams = 20;

const strings = {
  mustSelectAnArmyFirst: "Debes seleccionar un ejercito primero!",
  nowSelectAnotherToBattle: "Ahora selecciona otro ejercito para realizar una batalla",
};

function App() {
  let [armiesData, setArmiesData] = useState([]);
  let [deadArmiesData, setDeadData] = useState([]);
  let [battles, setBattles] = useState([]);
  let [armySelected, setArmySelected] = useState(undefined);
  let [training, setTraining] = useState(false);
  let [seeBattles, setSeeBattles] = useState(false);
  let [battlePressed, SetBattlePressed] = useState(false);
  let [loading, setLoading] = useState(false);

  const PressArmy = army => {
    let armies = armiesData;
    let deadArmies = deadArmiesData;
    let previousBattles = battles;
    if(battlePressed){
      let data = new Battle(armies, armySelected.id, army.id);
      for( let i = 0; i < data.armies.length; i++) {
        if (!data.armies[i].isAlive) {
          deadArmies.push(data.armies[i]);
          data.armies.splice(i, 1)
          data.armies.forEach((unid, index) => {
            data.armies[index].id = index
          })
        }
      }
      armies = data.armies;
      previousBattles.push(data.battle)
      setArmiesData(armies);
      setDeadData(deadArmies);
      setBattles(previousBattles);
      setSeeBattles(false)
      setLoading(true)
      setTimeout(() => {setLoading(false)}, 300)
    } else {
      setArmySelected(army)
    }
  };
  const PressTraining = () => {
    if(armySelected === undefined){
      alert(strings.mustSelectAnArmyFirst);
      return
    }
    setTraining(!training)
  };
  const PressBattle = () => {
    if(armySelected === undefined){
      alert(strings.mustSelectAnArmyFirst);
      return
    }
    SetBattlePressed(!battlePressed);
    alert(strings.nowSelectAnotherToBattle)
  };
  useEffect(() => {
    let defaultsArmies = new Array(quantityOfTeams).fill().map((e,i) => {
      let army = new Army();
      army.id=i;
      return army;
    });
    const deadArmies = [];
    let previousBattlesNumber = 140;
    let actualInitTeamForBattle = 0;
    let randomVictim = actualInitTeamForBattle;
    const previousBattles = new Array(previousBattlesNumber).fill().map((e,i) => {
      while(randomVictim === actualInitTeamForBattle){
        const randomNumber = Math.floor(Math.random() * (defaultsArmies.length - 1));
        if(defaultsArmies[randomNumber].isAlive){
          randomVictim = randomNumber;
        }
      }
      let data = new Battle(defaultsArmies, actualInitTeamForBattle, randomVictim);
      for( let i = 0; i < data.armies.length; i++) {
        if (!data.armies[i].isAlive) {
          deadArmies.push(data.armies[i]);
          data.armies.splice(i, 1)
          data.armies.forEach((unid, index) => {
            data.armies[index].id = index
          })
        }
      }
      defaultsArmies = data.armies;
      actualInitTeamForBattle ++;
      if(actualInitTeamForBattle >= data.armies.length){
        actualInitTeamForBattle = 0;
      }
      return data.battle
    });

    setArmiesData(defaultsArmies);
    setDeadData(deadArmies);
    setBattles(previousBattles);


  }, []);
  const SetNewArmySelected = (army) => {
    setArmySelected(army);
    let armiesForModify = armiesData;
    armiesData.forEach((armies, index) => {
      if(armies.id === army.id){
        armiesForModify[index] = army;
      }
    });
    setArmiesData(armiesForModify)
    setTraining(false)
    setTimeout(() => {setTraining(true)}, 200)
  };
  const PressBurbleBattles = () => {
    setSeeBattles(!seeBattles)
  };
  if(loading){
    return[
      <Battles setSeeBattles={PressBurbleBattles} />,
      <Footer battlePressed={battlePressed} PressBattle={PressBattle} PressTraining={PressTraining} />
    ]
  }
  if(seeBattles){
    return [
      <Battles setSeeBattles={PressBurbleBattles} />,
      <BattleHistory battles={battles} />,
      <Footer battlePressed={battlePressed} PressBattle={PressBattle} PressTraining={PressTraining} />
    ]
  }
  if(training){
    return [
      <Battles setSeeBattles={setSeeBattles} />,
      <ArmyForTraining SetNewArmySelected={SetNewArmySelected} armySelected={armySelected} />,
      <Footer battlePressed={battlePressed} PressBattle={PressBattle} PressTraining={PressTraining} />
    ]
  }
  return [
    <Battles setSeeBattles={setSeeBattles} />,
    <RenderArmies PressArmy={PressArmy} armies={armiesData} deads={deadArmiesData} />,
    <ArmySelected armySelected={armySelected} />,
    <Footer battlePressed={battlePressed} PressBattle={PressBattle} PressTraining={PressTraining} />
  ]
}

export default App;
