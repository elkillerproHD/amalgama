import React from 'react';
import styled from "styled-components";
import InglesesIMG from "../../images/ingleses.jpg";
import BizantinosIMG from "../../images/bizantinos.png";
import ChinosIMG from "../../images/china.png";

const strings = {
  bizantino: 'Bizantino',
  ingles: 'Ingles',
  chino: 'Chino',
  money: 'Dinero: $',
  power: 'Poder: '
};

const Container = styled.div `
  width: 15vw;
  height: 40vh;
  border: 1px solid #ededed;
  border-radius: 8px;
  background-color: white;
  -webkit-box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  &:hover {
    background-color: #ededed;
  }
  margin-left: 2%;
  margin-right: 2%;
  margin-top: 5%;
`;
const Image = styled.img `
  height: 80%;
`;
const ImageContainer = styled.div `
  width: 100%;
  height: 33%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const TextName = styled.p `
  font-size: 1.2em;
  font-weight: bold;
  color: black;
  text-align: center;
`;
const Text = styled.p `
  font-size: 0.8em;
  font-weight: bold;
  color: black;
  text-align: left;
  margin-left: 7%;
  margin-top: 5vh;
`;
export const Army = props => {
  let data = {};
  switch(props.army.type){
    case 0:
      // Chinos
      data.img = ChinosIMG;
      data.name = strings.chino;
      break;
    case 1:
      // Ingleses
      data.img = InglesesIMG;
      data.name = strings.ingles;
      break;
    case 2:
      // Bizantinos
      data.img = BizantinosIMG;
      data.name = strings.bizantino;
      break;
    default:
      break;
  }
  return(
    <Container onClick={()=>props.onClick(props.army)}>
      <ImageContainer>
        <Image src={data.img} />
      </ImageContainer>
      <TextName>
        {data.name}
      </TextName>
      <Text>
        {strings.money + props.army.money}
      </Text>
      <Text>
        {strings.power + props.army.power}
      </Text>
    </Container>
  )
};
