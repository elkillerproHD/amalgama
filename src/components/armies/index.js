import React from 'react';
import styled from "styled-components";
import {Army} from "./army";

const Container = styled.div `
  width: 80vw;
  height: 75vh;
  position: absolute;
  top: 10vh;
  left: 0;
  border-right: 1px solid #ededed;
  padding-left: 2%;
  padding-right: 2%;
  cursor: pointer;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;  
  overflow: auto;
  padding-bottom: 5vh;
`;

export const RenderArmies = (props) => {
  return (
    <Container>
      {
        props.armies.map((army) => {
          return <Army onClick={props.PressArmy} army={army} />
        })
      }
    </Container>
  )
};
