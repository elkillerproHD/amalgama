import React, {useState} from 'react';
import styled  from "styled-components";
import {Unit} from "../unit";
import PiquerosIMG from "../../images/duende-con-lanza.jpg";
import ArquerosIMG from "../../images/arquera.png";
import CaballerosIMG from "../../images/caballero.png";
import {Arquero} from "../../functions/army";

const strings = {
  archers: 'Arqueros',
  spearman: 'Piqueros',
  soldier: 'Caballero',
  level: 'Level: ',
  gold: 'Oro: ',
  upgrade: 'Entrenar',
  transform: 'Transformar',
  notMoney: 'Te quedaste sin dinero',
  cantTransformSoldier: 'No se puede transformar a un caballero'
};

const Container = styled.div `
  width: 100vw;
  height: 80vh;
  position: absolute;
  top: 10vh;
  left: 0;
  display: flex;
`;
const SubContainer = styled.div `
  width: 80vw;
  height: 80vh;
  display: flex;
  align-items: center;
  flex-direction: column;
  overflow: auto;
  padding-bottom: 5vh;
`;
const TextTitle = styled.p `
  font-size: 2em;
  font-weight: bold;
  color: black;
  text-align: left;
  margin-top: 10vh;
  width: 80vw;
  margin-left: 10vw;
`;
const Text = styled.p `
  font-size: 0.8em;
  font-weight: bold;
  color: black;
  text-align: left;
  margin-top: 10vh;
`;
const ContainerUnits = styled.div `
  width: 80vw;
  display: flex;
  justify-content: space-between;
  padding-left: 5%;
  padding-right: 5%;
  flex-wrap: wrap;
`;
const RightColumn = styled.div `
  width: 20vw;
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: #ededed;
`;
const Button = styled.button `
  width: 80%;
  height: 8vh;
  background-color: white;
  border-radius: 25px;
  -webkit-box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  margin-top: 3vh;
  font-size: 0.8em;
  font-weight: bold;
  color: black;
  border: none;
  cursor: pointer;
`;
const RenderSpearman = props => {
  const spearmansByLevel = [];
  const toRender = [];
  props.armySelected.piqueros.sort(function(a, b) {
    return a.power - b.power;
  });
  let level = 1;
  let levelContain = [];
  props.armySelected.piqueros.map((unit, index) => {
    if(unit.level === level){
      levelContain.push(unit);
    } else {
      spearmansByLevel.push({level: level, units: levelContain});
      levelContain = [];
      level = unit.level;
      levelContain.push(unit);
    }
    if(index + 1 === props.armySelected.piqueros.length){
      spearmansByLevel.push({level: level, units: levelContain});
      levelContain = [];
    }
  });
  console.log(spearmansByLevel)
  spearmansByLevel.forEach((data) => {
    toRender.push(<Text>{strings.level + data.level}</Text>);
    toRender.push(
      <ContainerUnits>
        {
          data.units.map((unit) => (
            <Unit onClick={props.click} unit={unit} />
          ))
        }
      </ContainerUnits>
    )
  });
  return toRender
};
const RenderArqueros = props => {
  const spearmansByLevel = [];
  const toRender = [];
  props.armySelected.arqueros.sort(function(a, b) {
    return a.power - b.power;
  });
  let level = 1;
  let levelContain = [];
  props.armySelected.arqueros.map((unit, index) => {
    if(unit.level === level){
      levelContain.push(unit);
    } else {
      spearmansByLevel.push({level: level, units: levelContain});
      levelContain = [];
      level = unit.level;
      levelContain.push(unit);
    }
    if(index + 1 === props.armySelected.arqueros.length){
      spearmansByLevel.push({level: level, units: levelContain});
    }
  });
  spearmansByLevel.forEach((data) => {
    toRender.push(<Text>{strings.level + data.level}</Text>);
    toRender.push(
      <ContainerUnits>
        {
          data.units.map((unit) => (
            <Unit onClick={props.click} unit={unit} />
          ))
        }
      </ContainerUnits>
    )
  });
  return toRender
};
const RenderCaballeros = props => {
  const spearmansByLevel = [];
  const toRender = [];
  props.armySelected.caballeros.sort(function(a, b) {
    return a.power - b.power;
  });
  let level = 1;
  let levelContain = [];
  props.armySelected.caballeros.map((unit, index) => {
    if(unit.level === level){
      levelContain.push(unit);
    } else {
      spearmansByLevel.push({level: level, units: levelContain});
      levelContain = [];
      level = unit.level;
      levelContain.push(unit);
    }
    if(index + 1 === props.armySelected.caballeros.length){
      spearmansByLevel.push({level: level, units: levelContain});
    }
  });
  spearmansByLevel.forEach((data) => {
    toRender.push(<Text>{strings.level + data.level}</Text>);
    toRender.push(
      <ContainerUnits>
        {
          data.units.map((unit) => (
            <Unit onClick={props.click} unit={unit} />
          ))
        }
      </ContainerUnits>
    )
  });
  return toRender
};

export const ArmyForTraining = props => {
  console.log(props)
  let [unitSelected, SetUnitSelected] = useState(undefined);
  const PressUnit = (unit) => {
    SetUnitSelected(unit)
  };
  const PressUpgrade = () => {
    console.log(unitSelected);
    let army = props.armySelected;
    switch(unitSelected.type){
      case 'Piquero':
        // Piquero
        army.piqueros.forEach((unid, index) => {
          if(army.money > 10){
            if(unid.id === unitSelected.id){
              army.piqueros[index].level += 1;
              army.piqueros[index].power += 3;
              army.money -= 10;
              SetUnitSelected(army.piqueros[index]);
            }
          } else {
            alert(strings.notMoney)
          }
        });
        break;
      case "Arquero":
        // Arquero
        army.arqueros.forEach((unid, index) => {
          if(army.money > 20) {
            if (unid.id === unitSelected.id) {
              army.arqueros[index].level += 1;
              army.arqueros[index].power += 7;
              army.money -= 20;
              SetUnitSelected(army.arqueros[index]);
            }
          } else {
            alert(strings.notMoney)
          }
        });
        break;
      case "Caballero":
        // Caballero
        army.caballeros.forEach((unid, index) => {
          if(army.money > 30) {
            if (unid.id === unitSelected.id) {
              army.caballeros[index].level += 1;
              army.caballeros[index].power += 10;
              army.money -= 30;
              SetUnitSelected(army.caballeros[index]);
            }
          } else {
            alert(strings.notMoney)
          }
        });
        break;
      default:
        break;
    }
    props.SetNewArmySelected(army)
  };
  const PressTransform = () => {
    console.log(unitSelected)
    let army = props.armySelected;
    switch(unitSelected.type){
      case 'Piquero':
        // Piquero
        army.piqueros.forEach((unid, index) => {
          if(army.money > 30){
            if(unid.id === unitSelected.id){
              army.piqueros.splice(index,1);
              army.money -= 30;
              SetUnitSelected(undefined);
              army.arqueros.push(new Arquero())
              army.piqueros.forEach((un, index) => {
                army.piqueros[index].id = index
              })
            }
          } else {
            alert(strings.notMoney)
          }
        });
        break;
      case "Arquero":
        // Arquero
        army.arqueros.forEach((unid, index) => {
          if(army.money > 40) {
            if (unid.id === unitSelected.id) {
              army.arqueros.splice(index,1);
              army.money -= 40;
              SetUnitSelected(undefined);
              army.caballeros.push(new Arquero())
              army.arqueros.forEach((un, index) => {
                army.arqueros[index].id = index
              })
            }
          } else {
            alert(strings.notMoney)
          }
        });
        break;
      case "Caballero":
        // Caballero
          alert(strings.cantTransformSoldier);
        break;
      default:
        break;
    }
    props.SetNewArmySelected(army)
  };
  return(
    <Container>
      <SubContainer>
        <TextTitle>{strings.spearman}</TextTitle>
        <RenderSpearman click={PressUnit} {...props} />
        <RenderArqueros click={PressUnit} {...props} />
        <RenderCaballeros click={PressUnit} {...props} />
      </SubContainer>
      <RightColumn>
        <Text>{strings.gold + props.armySelected.money}</Text>
        {
          unitSelected !== undefined && [
            <Unit onClick={()=>{}} unit={unitSelected} />,
            <Button onClick={PressUpgrade} >{strings.upgrade}</Button>,
            <Button onClick={PressTransform}>{strings.transform}</Button>
          ]
        }
      </RightColumn>
    </Container>
  )
};
