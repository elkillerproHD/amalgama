import React from 'react'
import styled from "styled-components";
import {Army} from "../armies/army";

const strings = {
  archers: 'Arqueros: ',
  soldier: 'Caballeros: ',
  spearman: 'Piqueros: '
};

const Container = styled.div `
  width: 20vw;
  height: 80vh;
  position: absolute;
  right: 0;
  top: 10vh;
`;
const SubContainer = styled.div `
  width: 20vw;
  height: 80vh;
  background-color: #ededed;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const Text = styled.p `
  font-size: 0.8em;
  font-weight: bold;
  color: black;
  text-align: left;
  margin-left: 7%;
  margin-top: 5vh;
`;
export const ArmySelected = props => {
  return(
    <Container>
      <SubContainer>
        {
          props.armySelected !== undefined && (
            <Army army={props.armySelected} />
          )
        }
        {
          props.armySelected !== undefined && [
            <Text>
              {strings.spearman + props.armySelected.piqueros.length}
            </Text>,
            <Text>
              {strings.archers + props.armySelected.arqueros.length}
            </Text>,
            <Text>
            {strings.soldier + props.armySelected.caballeros.length}
            </Text>
          ]
        }

      </SubContainer>
    </Container>
  )
};
