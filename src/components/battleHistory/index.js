import React from 'react';
import styled from "styled-components";
import {GetUnidades} from "../../functions/army";

const Container = styled.div `
  width: 100vw;
  height: 80vh;
  position: absolute;
  top: 10vh;
  left: 0;
  display: flex;
`;
const SubContainer = styled.div `
  width: 100vw;
  height: 70vh;
  display: flex;
  align-items: center;
  flex-direction: column;
  overflow: auto;
  padding-bottom: 5vh;
  padding-top: 5vh;
`;
const ArmyContainer = styled.div `
  width: 40vw;
  height: 10vh;
  border-radius: 10px;
  border: 1px solid #ededed;
  margin-left: 2vw;
  margin-right: 1vw;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;
const Battle = styled.div `
  width: 100vw;
  height: 10vh;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top:  5vh;
`;
const Text = styled.p `
  font-size: 0.8em;
  font-weight: bold;
  color: black;
`;

const constNames = ["Chinos", "Ingleses", "Bizantinos"];

export const BattleHistory = props => {
  console.log(props)
  return (
    <Container>
      <SubContainer>
        {
          props.battles.map((battle) => {
            return(
              <Battle>
                <ArmyContainer>
                  <Text>{"In Attack"}</Text>
                  <Text>{constNames[battle.attack.type]}</Text>
                  <Text>{"Power: " + battle.attack.power}</Text>
                  <Text>{"Money: " + battle.attack.money}</Text>
                </ArmyContainer>
                <ArmyContainer>
                  <Text>{"In Defend"}</Text>
                  <Text>{constNames[battle.attack.type]}</Text>
                  <Text>{"Power: " + battle.attack.power}</Text>
                  <Text>{"Money: " + battle.attack.money}</Text>
                </ArmyContainer>
              </Battle>
            )
          })
        }
      </SubContainer>
    </Container>

  )
};
