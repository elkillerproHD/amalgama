import React from 'react';
import styled from "styled-components";
import BookImage from "../../images/book.png"
import SwordImage from "../../images/sword.png"

const strings = {
  battleHistory: "Battle history"
};

const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
`;
const SubContainer = styled.div `
  width: 100vw;
  height: 10vh;
  border-bottom: 1px solid #ededed;
  position: relative;
  top: 0;
  left: 0;
  z-index: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const Burble = styled.div `
  width: 5vw;
  height: 5vw;
  z-index: 2;
  position: absolute;
  left: 47.5%;
  top: 45%;
  cursor: pointer;
`;
const BurbleSubContainer = styled.div `
  width: 5vw;
  height: 5vw;
  z-index: 2;
  border-radius: 10vw;
  border: 1px solid #ededed;
  position: relative;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  ${Burble}:hover & {
    background-color: #ededed;
    border: 1px solid #b7b7b7;
  }
`;
const BookImageStyle = styled.img `
  width: 3vw;
  height: 3vw;
`;
const SwordImageStyle = styled.img `
  width: 1.5vw;
  height: 1.5vw;
`;
const SwordContainer = styled.div `
  width: 2vw;
  height: 2vw;
  padding: 0.2vw;
  position: absolute;
  bottom: -1vw;
  right: -1vw;
  background-color: white;
  border-radius: 2vw;
  border: 1px solid #ededed;
  display: flex;
  justify-content: center;
  align-items: center;
  ${Burble}:hover & {
    background-color: #ededed;
    border: 1px solid #b7b7b7;
  }
`;
const Text = styled.p `
  font-size: 1.4em;
  font-weight: bold;
  color: black;
  margin: 0 0 5vh;
`;

export const Battles = (props) => {
  return (
    <Container >
      <SubContainer>
        <Text>{strings.battleHistory}</Text>
        <Burble onClick={props.setSeeBattles} >
          <BurbleSubContainer>
            <BookImageStyle src={BookImage} />
            <SwordContainer>
              <SwordImageStyle src={SwordImage} />
            </SwordContainer>
          </BurbleSubContainer>
        </Burble>
      </SubContainer>
    </Container>
  )
};
