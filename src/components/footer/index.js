import React from 'react';
import styled from "styled-components";
import SwordImg from '../../images/sword.png';
import DumbbellImg from '../../images/dumbbell.png';

const strings = {
  battles: 'Batallar',
  training: 'Training'
};

const Container = styled.div`
  width: 100vw;
  height: 10vh;
  position: absolute;
  left: 0;
  bottom: 0;
  background-color: white;
`;
const SubContainer = styled.div`
  width: 100vw;
  height: 10vh;
  border-top: 1px solid #ededed;
  display: flex;
`;
const columnStyle = `
  width: 50vw;
  height: 10vh;
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    background-color: #ededed;
  };
  cursor: pointer;
`;
const LeftColumn = styled.div `
  ${columnStyle};
  border-right: 1px solid #ededed;
`;
const LeftColumnActive = styled.div `
  ${columnStyle};
  border-right: 1px solid #ededed;
  background-color: #f9ffc9;
`;
const RightColumn = styled.div `
  ${columnStyle};
`;
const Icon = styled.img `
  width: 2vw;
  height: 2vw;
`;
const Text = styled.p `
  font-size: 2em;
  font-weight: bold;
  color: black;
  margin-left: 1vw;
`;
export const Footer = (props) => {
  return(
    <Container>
      <SubContainer>
        {
          props.battlePressed
          ? (
              <LeftColumnActive onClick={props.PressBattle}>
                <Icon src={SwordImg} />
                <Text>{strings.battles}</Text>
              </LeftColumnActive>
            )
          : (
              <LeftColumn onClick={props.PressBattle}>
                <Icon src={SwordImg} />
                <Text>{strings.battles}</Text>
              </LeftColumn>
            )
        }

        <RightColumn onClick={props.PressTraining}>
          <Icon src={DumbbellImg} />
          <Text>{strings.training}</Text>
        </RightColumn>
      </SubContainer>
    </Container>
  )
};
