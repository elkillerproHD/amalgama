import React, { useEffect, useCallback } from 'react';
import styled from "styled-components";
import ArquerosIMG from "../../images/arquera.png";
import PiquerosIMG from "../../images/duende-con-lanza.jpg";
import CaballerosIMG from "../../images/caballero.png";

const strings = {
  archers: 'Arqueros',
  spearman: 'Piqueros',
  soldier: 'Caballero',
  level: "Level: ",
  power: "Poder: "
};

const Container = styled.div `
  width: 15vw;
  height: 40vh;
  border: 1px solid #ededed;
  border-radius: 8px;
  background-color: white;
  -webkit-box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  -moz-box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.75);
  &:hover {
    background-color: #ededed;
  }
  margin-left: 2%;
  margin-right: 2%;
  margin-top: 3%;
`;
const Image = styled.img `
  height: 80%;
`;
const ImageContainer = styled.div `
  width: 100%;
  height: 33%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const TextName = styled.p `
  font-size: 1.2em;
  font-weight: bold;
  color: black;
  text-align: center;
`;
const Text = styled.p `
  font-size: 0.8em;
  font-weight: bold;
  color: black;
  text-align: left;
  margin-left: 7%;
  margin-top: 5vh;
`;
const ContainerUnits = styled.div `
  width: 90vw;
  display: flex;
  justify-content: space-between;
  padding-left: 5%;
  padding-right: 5%;
`;
export const Unit = props => {
  const [unit, setUnit] = React.useState({...props.unit});
  const [utils, setUtils] = React.useState({});

  const MakeUtils = () => {
    let data = {};
    switch(props.unit.type){
      case 'Piquero':
        // Piquero
        data.img = PiquerosIMG;
        data.name = unit.type;
        break;
      case "Arquero":
        // Arquero
        data.img = ArquerosIMG;
        data.name = unit.type;
        break;
      case "Caballero":
        // Caballero
        data.img = CaballerosIMG;
        data.name = unit.type;
        break;
      default:
        break;
    }
    setUtils(data)
  };
  React.useEffect(() => {
    setUnit(props.unit);
    MakeUtils();
  }, [props.unit]);
  return (
    <Container onClick={()=>props.onClick(unit)}>
      <ImageContainer>
        <Image src={utils.img} />
      </ImageContainer>
      <TextName>
        {utils.name}
      </TextName>
      <Text>
        {strings.level + unit.level}
      </Text>
      <Text>
        {strings.power + unit.power}
      </Text>
    </Container>
  )
};
