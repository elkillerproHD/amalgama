const powerOfPiquero = 5;
const powerOfArquero = 10;
const powerOfCaballero = 20;

export function Piquero(){
  this.level = 1;
  this.power = powerOfPiquero;
  this.type = 'Piquero';
}
export function Arquero(){
  this.level = 1;
  this.power = powerOfArquero;
  this.type = 'Arquero'
}
export function Caballero(){
  this.level = 1;
  this.power = powerOfCaballero;
  this.type = 'Caballero'
}

const armyChina = {
  piqueros: 2,
  arqueros: 25,
  caballeros: 2
};
const armyInglesa = {
  piqueros: 10,
  arqueros: 10,
  caballeros: 10
};
const armyBizantina = {
  piqueros: 5,
  arqueros: 8,
  caballeros: 15
};

export function GetUnidades(unids) {
  let totalPower = 0;
  const piqueros = new Array(unids.piqueros).fill().map((e,i) => {
    totalPower += powerOfPiquero;
    return new Piquero()
  });
  const arqueros = new Array(unids.arqueros).fill().map((e,i) => {
    totalPower += powerOfArquero;
    return new Arquero()
  });
  const caballeros = new Array(unids.caballeros).fill().map((e,i) => {
    totalPower += powerOfCaballero;
    return new Caballero()
  });
  return {
    piqueros,
    arqueros,
    caballeros,
    totalPower
  }
}

export function Battle(armies, initOfBattleTeam, armyInDefendStateTeam){
  const minimunDifferenceForWin = 50;
  let teamInit = armies[initOfBattleTeam];
  let teamInDefend = armies[armyInDefendStateTeam];
  let allUnits = [];
  function GetNewTeamAfterBattle (winner, loser, removeUnid, goldForWinner){
    loser.piqueros.map((unid, id) => {allUnits.push({id, ...unid})})
    loser.caballeros.map((unid, id) => {allUnits.push({id, ...unid})})
    loser.arqueros.map((unid, id) => {allUnits.push({id, ...unid})})
    allUnits.sort(function(a, b) {
      return a.power - b.power;
    });
    for(let i = 0; i < removeUnid; i++){
      allUnits.pop();
    }
    winner.money += goldForWinner;
    const newPiqueros = [];
    const newArqueros = [];
    const newCaballeros = [];
    allUnits.forEach((unit) => {
      if(unit.type === "Piquero"){
        newPiqueros.push(unit);
      } else if(unit.type === "Arquero"){
        newArqueros.push(unit);
      } else if(unit.type === "Caballero"){
        newCaballeros.push(unit);
      }
    });
    loser.piqueros = newPiqueros;
    loser.arqueros = newArqueros;
    loser.caballeros = newCaballeros;
    let totalPower = 0;
    loser.piqueros.forEach((e,i) => {
      totalPower += e.power;
    });
    loser.arqueros.forEach((e,i) => {
      totalPower += e.power;
    });
    loser.caballeros.forEach((e,i) => {
      totalPower += e.power;
    });
    loser.power = totalPower;
    if(loser.power === 0){
      loser.isAlive = false;
    }
    return {winner, loser};
  }
  let winner = {};
  if(teamInit.power < teamInDefend.power){
    let data = GetNewTeamAfterBattle(teamInDefend, teamInit, 2, 100);
    teamInDefend = data.winner;
    teamInit = data.loser;
    winner = data.winner;
  } else {
    const differenceOfPower = teamInit.power - teamInDefend.power;
    if(differenceOfPower < 50){
      let data = GetNewTeamAfterBattle(teamInDefend, teamInit, 1, 100);
      teamInDefend = data.winner;
      teamInit = data.loser;
      winner = data.winner;
    } else {
      let data = GetNewTeamAfterBattle(teamInit,teamInDefend, 2, 100);
      teamInDefend = data.loser;
      teamInit = data.winner;
      winner = data.winner;
    }
  }
  armies[initOfBattleTeam] = teamInit;
  armies[armyInDefendStateTeam] = teamInDefend;
  return {battle: { attack:{...teamInit, id: initOfBattleTeam}, defend: {...teamInDefend, id:armyInDefendStateTeam}, winner }, armies: armies};
}

export function Army() {
  this.money = 1000;
  this.type = Math.floor(Math.random() * 3);
  this.isAlive = true;
  // this.type = 0;
  let army = {};
  switch (this.type) {
    case 0:
      // Chinos
      army = GetUnidades(armyChina);
      break;
    case 1:
      // Ingleses
      army = GetUnidades(armyInglesa);
      break;
    case 2:
      // Bizantinos
      army = GetUnidades(armyBizantina);
      break;
    default:
      break;
  }
  this.piqueros = army.piqueros;
  this.arqueros = army.arqueros;
  this.caballeros = army.caballeros;
  this.power = army.totalPower;
}
